# movies-api-Soumyadev

Playing with movies Data

In the Directory Structure:
-> pgSQL/ 
    ->config.js = Setting up the environment variables and creating a instance pool by calling the Pool Constructor.
    ->poolConnect.js = Connected npm and psql. Created a function for doing pool request and exported it.
    ->fetchMoviesData = This is where I insert movies data in the database moviesData.

->workingExpressJS/
    ->expressConfig.js= Setting up the environment variables.
    ->connect.js = Connected npm and sql. Created a function for making psql queries.

->server/
    ->myLocalServer.js= Created a local server with Express JS. Imported all required objects from the above mentioned directory.For interaction with the backend we have used postman.