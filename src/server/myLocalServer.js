let connect= require('./workingExpressJS/connect.js')
const express = require('express')
const app = express()
const port=7001
const bodyParser=require('body-parser')
app.use(bodyParser.json())

//To manage the entire collection of movies resource
app.get('/api/directors', (req, res) =>{
    let query=`select * from director;`
    connect(query,res)
})
////Add new instance to director table
app.post('/api/directors', (req, res) =>{
    let data=req.body
    let columns=Object.keys(data)
    let value=Object.values(data)
    columns = columns.join(",")
    // console.log(columns.length)
    // console.log(value.length)
    let query=`insert into director(${columns}) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12);`
    connect(query,res,value)
})

////To manage the entire collection of directors resource
app.get('/api/movies', (req, res) =>{
    let query=`select rank,title from director;`
    connect(query,res)
})
//Add new instance to the movies table
app.post('/api/movies', (req,res) =>{
    let data=req.body
    let columns=Object.keys(data)
    let value=Object.values(data)
    columns=columns.join()
    //console.log(columns)
    //console.log(value)
    let query=`insert into movies(${columns}) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12);`
    connect(query,res,value)

})
////To manage a single movie resource

//To retrieve a movie
app.get('/api/movies/:movieId', (req,res)=> {
    let query='select * from movies where rank=$1'
    let value=[req.params.movieId]
    //console.log(req)
    //console.log(req.params)
    connect(query,res,value)
})

//To retrieve a director
app.get('/api/directors/:directorId', (req, res)=>{
    let query='select * from director where rank=$1'
    let value=[req.params.directorId]
    connect(query, res, value)
})

//To update details of a movie

app.put('/api/movies/:movieId', (req, res)=>{
    let data=req.body
    let column=Object.keys(data)
    let columnValues=Object.values(data)
    let setValue=""
    for(let i=0 ;i<column.length;i++){
        setValue+=column[i]+"="+columnValues[i]+","
    }
    setValue=setValue.slice(0,-1)
    //console.log(setValue)
    let query=`update movies set ${setValue} where rank=$1`
    let value=[req.params.movieId]
    connect(query, res, value)
})

//To update details of director
app.put('/api/directors/:directorId', (req, res)=>{
    //console.log(req)
    let data=req.body
    let column=Object.keys(data)
    let columnValues=Object.values(data)
    let setValue=""
    for(let i=0 ;i<column.length;i++){
        setValue+=column[i]+"="+columnValues[i]+","
    }
    setValue=setValue.slice(0,-1)
    //console.log(setValue)
    let query=`update director set ${setValue} where rank=$1`
    let value=[req.params.movieId]
    connect(query, res, value)
})

//To remove a movie : Delete query
app.delete('/api/movies/:movieId', (req, res)=> {
    let query=`delete from movies where rank=$1`
    let value=[req.params.movieId]
    connect(query, res, value)
})
//To remove a director : Delete query
app.delete('/api/directors/:directorId', (req, res)=> {
    let query=`delete from director where rank=$1`
    let value=[req.params.directorId]
    connect(query, res, value)
})
app.listen(port, () => console.log(`Example app listening on port ${port}!`))

