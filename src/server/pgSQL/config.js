const { Pool, Client } = require('pg')

const pool = new Pool({
  user: process.env.USER,
  host: 'localhost',
  database: 'moviesData',
  password: 6897,
  port: 5432,
  max:100
})
//console.log("Inside envConfig")
pool.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err)
  process.exit(-1)
})
module.exports=pool
