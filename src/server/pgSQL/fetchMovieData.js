const fs= require('fs')
const pooling=require("./config.js")
let data=fs.readFileSync('../../data/movies.json','utf-8')
data=JSON.parse(data)
data.map((obj)=>{
    for(let i in obj){
        if(typeof obj[i]==='string'){
            if(obj[i]=="NA"){
                obj[i]=0
            }else if(obj[i].includes("'")){
                obj[i]=obj[i].replace("'"," ")
            }
            obj[i]="'"+obj[i]+"'"  
        }
    }
})
let connect= require('./poolConnect.js')
let query1= `CREATE TABLE IF NOT EXISTS director(director varchar,id serial primary key)`
let query3= `CREATE TABLE IF NOT EXISTS movies(Rank int, Title varchar, Description varchar, Runtime int, Genre varchar, Rating float, Metascore int, Votes int, Gross_Earning_in_Mil float, Director varchar, Actor varchar, Year int, id int references director(id) on delete cascade);`
let directorArr=Array.from(new Set(data.map(obj=>obj.Director)))
let query2=`INSERT INTO director (director) values(${directorArr})`
connect(query1).then(()=>{
    (function(){
        return new Promise((resolve,reject)=>{
            directorArr.map((n)=>{
                let query2=`insert into director (director) values(${n})`
                connect(query2)
            }) 
            if(directorArr.length!=0){
                resolve()           
            }else{
                reject("Empty Array!")
            }

        })
    })().then(()=>{
        connect(query3).then(()=>{
            data.forEach((obj)=>{
                let query4=`select id,director from director where director=${obj.Director}`
                pooling
                .connect()
                .then(client => {
                    console.log(query4)
                    return client
                    .query(query4)
                    .then(result => {
                        obj["id"]=result.rows[0].id
                        let moviesValue=Object.values(obj)
                        let query5=`insert into movies values(${moviesValue})`
                        connect(query5)
                        client.release()
                        
                    // console.log(res.rows[0])
                    })
                    .catch(err => {
                        client.release()
                        console.log(err.stack)
                    })
                }).catch(err=>{
                    console.log(err)
                // pool.end()
                })
            })
        })
    })
})



// connect(query1)
//     .then(()=>{
//         data.forEach((obj)=>{
//             let value=Object.values(obj).map((n)=>{
//             if(typeof n==='string'){
//                 if(n==="NA"){
//                     n=0
//                     return n
//                 }else if(n.indexOf("'")!=-1){
//                     n=n.replace("'"," ")
//                 }
//                 return "'"+n+"'"
//             }
//             return n
//         })
//         let query2= `INSERT INTO director VALUES(${value});`
//         connect(query2).then((pool)=>{
            
//         })
//     })
// })
// // .then((pool)=>{
// //     pool.end()
// // })

