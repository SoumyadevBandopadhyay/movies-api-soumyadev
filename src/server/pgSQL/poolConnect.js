module.exports = function (queryString){

    return new Promise((resolve,reject)=>{
        
        let pool= require('./config.js')
        let q=queryString
        //console.log(q)
        // the pool will emit an error on behalf of any idle clients
        // it contains if a backend error or network partition happens
        // pool.on('error', (err, client) => {
        //     console.error('Unexpected error on idle client', err)
        //     process.exit(-1)
        // })
      
        pool
        .connect()
        .then(client => {
            console.log(q)
            return client
            .query(q)
            .then(res => {
                client.release()
                resolve(pool);
                
               // console.log(res.rows[0])
            })
            .catch(err => {
                client.release()
                console.log(err.stack)
            })
        }).catch(err=>{
            console.log(err)
           // pool.end()
        })
    })
    // resolve("hello")

}

